<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvoiceIdToPurchaseProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('purchase_products', function (Blueprint $table) {
           $table->integer('reminder_number')->default(0);
           $table->string('invoice_id')->nullable()->unique();
           $table->string('type')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('purchase_products', function (Blueprint $table) {
        $table->dropColumn('reminder_number');
        $table->dropColumn('invoice_id');
        $table->dropColumn('type');
    });
    }
}
