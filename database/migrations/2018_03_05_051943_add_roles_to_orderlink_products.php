<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRolesToOrderlinkProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
              Schema::table('orderlink_products', function (Blueprint $table) {
           
             $table->integer('role_id')->unsigned()->nullable();
                 $table->foreign('role_id')->references('id')->on('roles')
                ->onUpdate('cascade')->onDelete('cascade');
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
           Schema::table('orderlink_products', function (Blueprint $table) {
            $table->dropColumn('role_id');
        });
    }
}
