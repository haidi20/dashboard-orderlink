<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWALinkStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('w_a_link_statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('walink_id')->unsigned();
            $table->string('ip')->nullable();
            $table->string('user_agent')->nullable();
            $table->string('browser')->nullable();
            $table->string('os')->nullable();
            $table->string('device')->nullable();
            $table->string('resolution')->nullable();
            $table->integer('touch_point')->nullable();
            $table->integer('rtt')->nullable();

            $table->string('referrer')->nullable();

            $table->string('source')->nullable();
            $table->string('medium')->nullable();
            $table->string('term')->nullable();
            $table->string('content')->nullable();
            $table->string('campaign')->nullable();
            
            $table->string('visitor_id')->nullable();

            $table->boolean('click_button')->default(false);

            $table->string('country')->nullable();
            $table->string('region')->nullable();
            $table->string('city')->nullable();

            $table->string('isp')->nullable();





            $table->timestamps();

               $table->foreign('walink_id')->references('id')->on('w_a_links')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('w_a_link_statistics');
    }
}
