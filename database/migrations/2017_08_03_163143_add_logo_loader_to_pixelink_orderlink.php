<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLogoLoaderToPixelinkOrderlink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
      Schema::table('orderlinks', function (Blueprint $table) {
        $table->string('logo_loader')->default('loader');
    });
      Schema::table('pixelinks', function (Blueprint $table) {
        $table->string('logo_loader')->default('loader');
    });
  }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
     Schema::table('orderlinks', function (Blueprint $table) {
        $table->dropColumn('logo_loader');
    });
     Schema::table('pixelinks', function (Blueprint $table) {
        $table->dropColumn('logo_loader');
    });
 }
}
