<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderlinkStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderlink_stats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('orderlink_id')->unsigned();
            $table->string('ip');
            $table->string('user_agent');
            $table->string('platform');
            $table->string('browser');
            $table->string('source')->nullable();
            $table->string('medium')->nullable();
            $table->string('term')->nullable();
            $table->string('content')->nullable();
            $table->string('campaign')->nullable();
            $table->string('referrer')->nullable();
            $table->string('fingerprint');
            $table->timestamps();

        $table->foreign('orderlink_id')->references('id')->on('orderlinks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderlink_stats');
    }
}
