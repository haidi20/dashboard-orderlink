<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProtocolAndReplaceTagToPageBuilder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
              Schema::table('landing_page_v3s', function (Blueprint $table) {
           
            $table->string('protocol')->nullable();
            $table->longtext('list_tag')->nullable();
                  $table->longtext('template_json')->nullable()->change();
             $table->longtext('template_html')->nullable()->change();
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
           Schema::table('landing_page_v3s', function (Blueprint $table) {
            $table->dropColumn('protocol');
             $table->dropColumn('list_tag');
        });
    }
}
