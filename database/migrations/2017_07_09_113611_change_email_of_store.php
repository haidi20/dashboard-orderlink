<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeEmailOfStore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
          Schema::table('stores', function (Blueprint $table) {
        $table->dropColumn('need_confirm_email');
        $table->dropColumn('order_confirmed_email');
        $table->dropColumn('order_paid_email');
        $table->dropColumn('payment_reminder_email');
        $table->dropColumn('order_canceled_email');
    });
        Schema::table('stores', function (Blueprint $table) {
            $table->longText('need_confirm_email')->nullable();
            $table->longText('order_confirmed_email')->nullable();
            $table->longText('order_paid_email')->nullable();
            $table->longText('payment_reminder_email')->nullable();
            $table->longText('order_canceled_email')->nullable();

            $table->string('need_confirm_email_header')->nullable();
            $table->string('order_confirmed_email_header')->nullable();
            $table->string('order_paid_email_header')->nullable();
            $table->string('payment_reminder_email_header')->nullable();
            $table->string('order_canceled_email_header')->nullable();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('stores', function (Blueprint $table) {
        $table->dropColumn('need_confirm_email_header');
        $table->dropColumn('order_confirmed_email_header');
        $table->dropColumn('order_paid_email_header');
        $table->dropColumn('payment_reminder_email_header');
        $table->dropColumn('order_canceled_email_header');
    });
   }
}
