<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewOrderlinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_orderlinks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url')->unique();
            $table->string('phone');
            $table->string('message')->nullable();
            $table->string('title')->nullable();
            $table->string('page_template')->nullable();
            $table->string('tipelink');
            $table->string('desktop_url')->nullable();
            $table->string('api_version')->default('4.0');
            $table->integer('redirect_time')->default(0);
            $table->string('logo_loader')->nullable();
            $table->string('text_loader')->nullable();
            $table->integer('author_id')->unsigned();
            $table->integer('embed_script_id')->unsigned()->nullable();
            $table->foreign('embed_script_id')->references('id')->on('embed_scripts')->onDelete('cascade');
            $table->foreign('author_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_orderlinks');
    }
}
