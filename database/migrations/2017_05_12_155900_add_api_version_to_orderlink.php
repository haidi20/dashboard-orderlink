<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApiVersionToOrderlink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('orderlinks', function (Blueprint $table) {
        $table->string('api_version')->nullable();
        });

        Schema::table('pixelinks', function (Blueprint $table) {
        $table->string('api_version')->nullable();
        });

        Schema::table('c_campaigns', function (Blueprint $table) {
        $table->string('api_version')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('orderlinks', function (Blueprint $table) {
        $table->dropColumn('api_version');
        });

        Schema::table('pixelinks', function (Blueprint $table) {
        $table->dropColumn('api_version');
        });

        Schema::table('c_campaigns', function (Blueprint $table) {
         $table->dropColumn('api_version');
        });
    }
}
