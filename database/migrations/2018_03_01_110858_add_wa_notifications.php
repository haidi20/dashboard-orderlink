<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWaNotifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('stores', function (Blueprint $table) {
            $table->string('checkout_wa_notif')->nullable();
            $table->string('payment_reminder_wa_notif')->nullable();
            $table->string('paid_wa_notif')->nullable();
            $table->string('cancel_wa_notif')->nullable();
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('stores', function (Blueprint $table) {
            $table->dropColumn('checkout_wa_notif');
            $table->dropColumn('payment_reminder_wa_notif');
            $table->dropColumn('paid_wa_notif');
            $table->dropColumn('cancel_wa_notif');
        });
    }
}
