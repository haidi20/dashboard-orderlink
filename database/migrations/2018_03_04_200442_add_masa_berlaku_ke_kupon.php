<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMasaBerlakuKeKupon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
             Schema::table('orderlink_product_coupons', function (Blueprint $table) {
           
            $table->dateTime('valid_until')->nullable();
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('orderlink_product_coupons', function (Blueprint $table) {
            $table->dropColumn('valid_until');
        });
    
    }
}
