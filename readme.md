# Orderlink Repository!

ini adalah repo orderlink yang baru. Mengapa ga pake Orderlink yang lama? Mengapa Harus di rework?

karena Repo yang lama strukturnya acak-acakan, khas Programmer Newbie. Akan susah jika harus di Share ke orang lain untuk menjelaskan setiap detil struktur filenya.

so, diputuskanlah kita memulai kembali dari awal.

**Installation**

 1. download/clone repo ini

 2. `composer install`

 3.  `npm install`

 4.  `php artisan migrate --seed`

 5.  selesai.
 

struktur file dari repo ini sebagian besar sama dengan Laravel Fresh 5.6. bedanya sebagai berikut

1. sudah ada migration dan seeder, so tinggal migrate aja!
2. menambah config tambahan pada `webpack.mix.js`
3. components dari vuely yang sudah ditambahkan di `resources/js/`

![enter image description here](http://res.cloudinary.com/resensi-digital/image/upload/v1533395434/struktur_file_jn9cic.jpg)

itu struktur baru dari resources/js, berikut penjelasannya.
**api** 
abaikan saja

**assets**
abaikan saja

**auth**
abaikan saja, kita tidak pke auth0.com

**components**
berisi components dari vuely

**firebase**
abaikan, kita tidak pke firebase

**helpers**
berisi fungsi-fungsi sederhana seperti getTheDate (dapatkan tanggal sesuai format tertentu),  convertDateToTimeStamp, textTruncate, hexToRgbA, getCurrentAppLayout

**lang**
vuely menggunakan multilanguage, jadi text-text seperti button, title, dll. itu dikustomisasi berdasarkan bahasa yang dipilih oleh user. contoh penggunaan lang pada title menu. message.dashboard dan message.ecommerce adalah variabel pada lang.

      export  const  category1  = [
    {
	    action:  'zmdi-view-dashboard',
	    title:  'message.dashboard',
	    items: [
		    { title:  'message.ecommerce', path:  '/other-page', exact:  true },
		    ]
	    }
    ]

pattern ini harus kita jaga, jadi nanti buat 1 lang lagi untuk bahasa indonesia.

**lib**
untuk meletakkan css,script library/plugin external

**router**
router sudah saya buat standar seperti vue-router dasar

**store**
store adalah vuex module berisi dari settingan awal seperti thema yang dipilih. we will use it latter!

**themes**
themes adalah komposisi struktur warna dasar 

    export  default {
    primary:  '#00D0BD',
    secondary:  '#424242',
    accent:  '#82B1FF',
    error:  '#FF3739',
    info:  '#5D92F4',
    success:  '#00D014',
    warning:  '#FFB70F'
    }

**views**
views adalah daftar pages yang dibuat

**app.js**
tempat semuanya bermula

**App.vue**
component pertama yang diload oleh apps.js dan dipanggil ke welcome.blade.php, berisi header, menu, sidebar, dll.

**bootstrap.js**
default laravel bootstrap.js, just delete jquery dan bootstrap

**globalComponents.js**
berisi daftar component yang bisa dipanggil dimanapun, cukup tulis tagnya, tanpa perlu import lagi.


