import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);
const routes = [{
  path: "/",
  component: require("Views/Home.vue")
},{
  path: "/other-page",
  component: require("Views/OtherPage.vue")
}
];

export default new VueRouter({
  routes
});
