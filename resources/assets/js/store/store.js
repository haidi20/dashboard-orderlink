import Vue from 'vue'
import Vuex from 'vuex'

// modules
import settings from './modules/settings';
Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        settings,
    }
})
