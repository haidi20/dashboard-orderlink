/**
 * App Entry File
 * Vuely: A Powerfull Material Design Admin Template
 * Copyright 2018. All Rights Reserved
 * Created By: The Iron Network, LLC
 * Made with Love
 */

 require('./bootstrap')


import Vue from 'vue'
import Vuetify from 'vuetify'
import VueBreadcrumbs from 'vue2-breadcrumbs'
import VueResource from 'vue-resource'
import VueNotifications from 'vue-notifications'
import miniToastr from 'mini-toastr'
import Nprogress from 'nprogress'
import VueI18n from 'vue-i18n'
import VueTour from 'vue-tour'
import fullscreen from 'vue-fullscreen'
import $ from 'jquery';

// global components
import GlobalComponents from './globalComponents'

// app.vue
import App from './App.vue'

// router
import router from './router'

// themes
import primaryTheme from './themes/primaryTheme';

// store
import { store } from './store/store';

// firebase
import './firebase'

// include script file
import './lib/VuelyScript'

// include all css files
import './lib/VuelyCss'

// messages
import messages from './lang';

// navigation guards before each
router.beforeEach((to, from, next) => {
	Nprogress.start()
		next() // make sure to always call next()!
	
})

// navigation guard after each
router.afterEach((to, from) => {
	Nprogress.done()
	setTimeout(() => {
		$('.v-content__wrap').scrollTop(0, 0)
		$('.app-boxed-layout .app-content').scrollTop(0, 0)
		$('.app-mini-layout .app-content').scrollTop(0, 0)
	}, 200);
})

function toast({ title, message, type, timeout, cb }) {
	return miniToastr[type](message, title, timeout, cb)
}

const options = {
	success: toast,
	error: toast,
	info: toast,
	warn: toast
}

const toastTypes = {
	success: 'success',
	error: 'error',
	info: 'info',
	warn: 'warn'
}

miniToastr.init({ types: toastTypes })

// plugins
Vue.use(Vuetify, {
	theme: store.getters.selectedTheme.theme
});

Vue.use(VueI18n)
Vue.use(VueTour)
Vue.use(VueResource)
Vue.use(VueBreadcrumbs)
Vue.use(VueNotifications, options)
Vue.use(fullscreen);
Vue.use(GlobalComponents);


// Create VueI18n instance with options
const i18n = new VueI18n({
	locale: store.getters.selectedLocale.locale, // set locale
	messages, // set locale messages
})


/* eslint-disable no-new */
new Vue({
	store,
	el: '#app',
	i18n,
	router,
	components: { App },
	mounted(){
		Nprogress.done()
	}
})
